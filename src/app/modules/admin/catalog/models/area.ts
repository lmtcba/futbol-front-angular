import { Upload } from "src/app/core/models/uploads/upload";

export interface Area {
  id: string;
  name: string;
  code: string;
  flag: string;
  parentAreaId: string;
  parentArea: string;
  uploadRequest?: Upload;
}
