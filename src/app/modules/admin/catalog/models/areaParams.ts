import { PaginatedFilter } from 'src/app/core/models/Filters/PaginatedFilter';

export class AreaParams implements PaginatedFilter {
  searchString: string;
  areaId: number;
  pageNumber: number;
  pageSize: number;
  orderBy: string;
}
