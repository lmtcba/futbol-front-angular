import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from 'src/app/core/guards/permission.guard';
import { AreaComponent } from './area/area.component';

const routes: Routes = [
  {
    path: 'areas',
    component: AreaComponent,
    canActivate: [PermissionGuard],
    data: {
      allowedPermissions: ['Permissions.Areas.Search']
    }
  },
  {
    path: 'areas',
    component: AreaComponent,
    canActivate: [PermissionGuard],
    data: {
      allowedPermissions: ['Permissions.Areas.View']
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule {
}
