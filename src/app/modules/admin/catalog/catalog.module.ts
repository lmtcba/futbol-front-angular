import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CatalogRoutingModule} from './catalog-routing.module';
import {CatalogComponent} from './catalog.component';
import {MaterialModule} from 'src/app/core/material/material.module';
import {SharedModule} from 'src/app/core/shared/shared.module';
import { AreaService } from 'src/app/modules/admin/catalog/services/area.service';
import { AreaComponent } from './area/area.component';

@NgModule({
  declarations: [
    CatalogComponent,
    AreaComponent
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    MaterialModule,
    SharedModule
  ],
  providers: [
    AreaService
  ]
})
export class CatalogModule {
}
