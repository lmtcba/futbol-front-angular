import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AreaService } from '../services/area.service';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss']
})
export class AreaComponent implements OnInit {
  areaForm: FormGroup;
  formTitle: string = "Areas Management";
  private areaService: AreaService;
  constructor() { }

  ngOnInit(): void {
  }
  onSubmit() {
    if (this.areaForm.valid) {

    }
  }
}
