import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AreaApiService } from 'src/app/core/api/catalog/area-api.service';
import { Upload } from 'src/app/core/models/uploads/upload';
import { IResult } from 'src/app/core/models/wrappers/IResult';
import { PaginatedResult } from 'src/app/core/models/wrappers/PaginatedResult';
import { Result } from 'src/app/core/models/wrappers/Result';
import { Area } from '../models/area';
import { AreaParams } from '../models/areaParams';

@Injectable()
export class AreaService {
  constructor(private api: AreaApiService) {}

  getAreas(
    areaParams: AreaParams
  ): Observable<PaginatedResult<Area>> {
    let params = new HttpParams();
    if (areaParams.searchString) {
      params = params.append('searchString', areaParams.searchString);
    }
    if (areaParams.pageNumber) {
      params = params.append('pageNumber', areaParams.pageNumber.toString());
    }
    if (areaParams.pageSize) {
      params = params.append('pageSize', areaParams.pageSize.toString());
    }
    if (areaParams.orderBy) {
      params = params.append('orderBy', areaParams.orderBy.toString());
    }
    return this.api
      .getAlls(params)
      .pipe(map((response: PaginatedResult<Area>) => response));
  }

  getAreaById(id: string): Observable<Result<Area>> {
    return this.api.getById(id).pipe(map((response: Result<Area>) => response));
  }

  getAreaImageById(id: string): Observable<Result<string>> {
    return this.api.getImageById(id).pipe(map((response: Result<string>) => response));
  }

  createArea(area: Area, upload: Upload): Observable<IResult<Area>> {
    if (upload != undefined && upload.data != undefined) area.uploadRequest = upload;
    return this.api
      .create(area)
      .pipe(map((response: IResult<Area>) => response));
  }

  updateArea(area: Area, upload: Upload): Observable<IResult<Area>> {
    console.log(upload);
    if (upload != undefined && upload.data != undefined) area.uploadRequest = upload;
    return this.api
      .update(area)
      .pipe(map((response: IResult<Area>) => response));
  }

  deleteArea(id: string): Observable<IResult<string>> {
    return this.api
      .delete(id)
      .pipe(map((response: IResult<string>) => response));
  }
}
