import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import { Area } from 'src/app/modules/admin/catalog/models/area';
import {environment} from 'src/environments/environment';
import { Result } from '../../models/wrappers/Result';

@Injectable()
export class AreaApiService {

  baseUrl = environment.apiUrl + 'catalog/areas/';

  constructor(private http: HttpClient) {
  }

  getAlls(params: HttpParams) {
    return this.http.get(this.baseUrl, {params: params});
  }

  getById(id: string) {
    return this.http.get<Result<Area>>(this.baseUrl + id);
  }

  getImageById(id: string) {
    return this.http.get(this.baseUrl + `image/${id}`);
  }

  create(area: Area) {
    return this.http.post(this.baseUrl, area);
  }

  update(area: Area) {
    return this.http.put(this.baseUrl, area);
  }

  delete(id: string) {
    return this.http.delete(this.baseUrl + id);
  }
}
